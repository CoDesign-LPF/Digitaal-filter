# CocaCoDesign
Driver board for JDB CoDesign audiocdec. (see HvA codesign 2016 - 2017) 
I2S driver using DMA and ISR data handling for maximum performance.


## The repository
The repository contains the firmware and hardware files for the CocaCoDesign board.
The board has two microcontrollers, a ATSAMG55 and a ATXMEGA32E5. The main MCU 
(SAMG55) ships with a bootloader which can be controlled using the secondary 
MCU, the Xmega32E5. The Xmega32E5 has an Xboot bootloader for firmware updates
over UART.
The bootloader for the SAMG55 is made by Atmel (Microchip) and the commands
have not been implemented in the XMEGA32E5 yet.

## SAMG55 firmware
Upon boot the device starts initializing its features, the clock, UART, I2S and if
set the the toolchain the floating point unit. Upon starting the I2S with DMA the
peripheral starts ping-pong'ing buffers. ping-pong buffering is double buffering the
data, when one buffer is being read the other is being filled and they switch every 
cycle.