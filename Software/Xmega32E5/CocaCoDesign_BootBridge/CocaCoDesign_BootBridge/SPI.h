/*
 * SPI.h
 *
 * Created: 18-Mar-18 00:36:41
 *  Author: Willem van der Kooij
 */ 


#ifndef SPI_H_
#define SPI_H_

void Init_SPI(void);
uint8_t SPI_xchg(uint8_t txdata);
void BootSpiXchg(uint8_t *txdata, uint8_t *rxdata, uint16_t len);


#endif /* SPI_H_ */