/*
 * CocaCoDesign_BootBridge.c
 *
 * Created: 18-Mar-18 00:02:47
 * Author : Willem van der Kooij
 *
 *
 * \note changed clksys_driver.c to make file work without 2MHz RC OSC
 */ 

#define F_CPU 32000000

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "avr_compiler.h"
#include "clksys_driver.h"
#include "UART_Debug.h"
#include "SPI.h"
#include "Xmodem.h"
#include "BootBridge.h"

static void Init_IO(void);
static void Init_Clock(void);


int main(void)
{	
	uint32_t ledToggleFlag = 0;
	
	Init_Clock();
	Init_IO();	
	Init_UART();
	Init_SPI();

	sei();
				
    while (1) {
		
		if (ledToggleFlag == 0x0007FFFF) {
			PORTA.OUTTGL = PIN1_bm;
			ledToggleFlag = 0;
		} else ledToggleFlag++;
		
		
		if(CanRead_Ctrl()) {			
			switch(ReadByte_Ctrl()) {
				case 0x1B:			/* ESC, reset chip */
					CCPWrite( &RST.CTRL, RST_SWRST_bm );
					break;
				case 'b':			/* do some blinky thingy */
				case 'B':
					PORTA.OUTTGL = PIN1_bm;
					break;
				case 'l':
				case 'L':
					PORTA.OUTTGL = PIN1_bm | PIN2_bm;
					break;
				case 'j':			/* Joe Praktiscchhhhh */
				case 'J':
					printf("Joe Praktisch hoiii\r\n");
					break;
				case 'x':			/* Activate BootBridge mode (Xmodem) */
				case 'X':	
					PORTC.DIRSET = PIN3_bm;			//hold slave reset
					PORTC.OUTCLR = PIN3_bm;			//reset slave
					_delay_ms(50);
					PORTC.OUTSET = PIN3_bm;			//hold reset line high
					
					SAMG55_BootProg();
					
					PORTC.OUTCLR = PIN3_bm;			//reset slave device
					_delay_ms(50);
					PORTC.DIRCLR = PIN3_bm;			//let go of reset line, pullup on line should pull line high again
					break;
				default:
					break;
			}
		}	
				
	}
}

/*
 *	\brief Initialize IO ports
 *	\details Setup In -and Outputs
 */
static void Init_IO(void)
{
	PORTA.DIRSET =	PIN1_bm | PIN2_bm;	//debug leds
	//PORTA.OUTSET =	PIN1_bm;	//light them debugs up

	PORTC.DIRCLR = PIN2_bm;			//slave handshake
	
	//let go of reset line by default
	PORTC.DIRCLR = PIN3_bm;
	PORTC.OUTCLR = PIN3_bm;
}


/*
 *	\brief Setup Main clock (F_CPU)
 *	\details Run Main Clock @ 32MHz from external oscillator
 */
static void Init_Clock(void)
{
	CLKSYS_XOSC_Config( OSC_FRQRANGE_12TO16_gc, 0, OSC_XOSCSEL_XTAL_16KCLK_gc );
	CLKSYS_Enable( OSC_XOSCEN_bm );
	do {} while ( CLKSYS_IsReady( OSC_XOSCRDY_bm ) == 0 );		
	CLKSYS_PLL_Config( OSC_PLLSRC_XOSC_gc, 2);
	CLKSYS_Enable( OSC_PLLEN_bm );
	do {} while ( CLKSYS_IsReady( OSC_PLLRDY_bm ) == 0 );
	CLKSYS_Main_ClockSource_Select( CLK_SCLKSEL_PLL_gc );
	CLKSYS_Disable( OSC_RC2MEN_bm );
	CLKSYS_Disable( OSC_RC32MEN_bm );
	// CLKSYS_AutoCalibration_Enable(OSC_RC32MCREF_bm, 1);
	
	
		/* //use external crystal to run @ 32MHz from PLL
	OSC.XOSCCTRL =	OSC_FRQRANGE_12TO16_gc
				|	OSC_XOSCSEL_XTAL_16KCLK_gc;
	
	OSC.CTRL = OSC_XOSCEN_bm;				//enable external OSC
	while (!(OSC_STATUS & OSC_XOSCRDY_bm));	//wait for clock to be stable
	
	// 16MHz XOSC * 2 for 32MHz OSC 
	OSC.PLLCTRL =	OSC_PLLSRC_XOSC_gc
				|	OSC_PLLFAC2_bm;
				
	OSC.CTRL |= OSC_PLLEN_bm;	//enable PLL
	while (!(OSC_STATUS & OSC_PLLRDY_bm));	//wait for PLL clock to be stable
		
	//CCP = CCP_IOREG_gc;	
		
	//set PLL clock as main clock by disabling XOSC
	OSC.CTRL &= ~(OSC_XOSCEN_bm);
	*/
	
}
