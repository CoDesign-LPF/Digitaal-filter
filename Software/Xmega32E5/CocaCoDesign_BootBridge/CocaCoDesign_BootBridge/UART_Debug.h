/*
 * UART_Debug.h
 *
 * Created: 28-Jan-18 00:54:47
 *  Author: Willem van der Kooij
 */ 


#ifndef UART_DEBUG_H_
#define UART_DEBUG_H_

extern FILE gDebug;

void Init_UART(void);

uint8_t CanRead_Ctrl(void);
uint8_t ReadByte_Ctrl(void);
void Debug_FlushBuffer(void);

uint8_t sci_drdy(void);
uint8_t sci_getData(void);
void sci_writeData(uint8_t txdata);

uint8_t CanWrite_Ctrl(void);
void WriteByte_Ctrl(uint8_t data);

#endif /* UART_DEBUG_H_ */