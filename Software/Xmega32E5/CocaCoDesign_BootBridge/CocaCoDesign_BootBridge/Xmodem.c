 // DA werkt niet
// http://www.menie.org/georges/embedded/xmodem.html

/******************************************************************************
 * RX63N - Xmodem module
 ******************************************************************************/
#define F_CPU 32000000

#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "xmodem.h"
#include "UART_Debug.h"

static uint8_t xm_buf[XM_BUFFER_SIZE + 5]; /* 3 header, 1024 data, 2 crc */


xm_err_t xmodem_receive(bool (*callback)(void *p_args), uint32_t offset) 
{
	uint8_t c, retry = XM_RETRY_LIMIT, response = 'C', crcflag = 0, xm_pkt_num = 1;
	uint16_t i, timeout, xm_pkt_size;
	uint32_t ofs = offset;
	xm_data_t xm_data;

	Debug_FlushBuffer();
	cli();

	while (retry > 0) {
		sci_writeData(response);

		/* Wait for start of packet */
		timeout = XM_TIMEOUT_DELAY;
		while (timeout && (!sci_drdy())) {
			_delay_us(XM_RXSCAN_DELAY_US);
			timeout--;
		}
		
		if (timeout) {		/* response received in time */
			c = sci_getData();
			switch(c){
    			case SOH:                   /* xmodem header received */
    				xm_pkt_size = 128;
    				break;
    			case STX:                   /* xmodem 1k header received */
    				xm_pkt_size = 1024;
    				break;
    			case EOT:                   /* transmission complete */
    				Debug_FlushBuffer();
    				sci_writeData(ACK);
    				sci_writeData(ACK);
    				return (XM_SUCCESS);
    			case CAN:                   /* transmission canceled by host */
    				Debug_FlushBuffer();
    				sci_writeData(ACK);
    				return (XM_ERR_CAN);
    			default:
    				break;
			}
		} else {			/* timed out, try again */
			retry--;
			continue;
		}
		
		/* check if CRC mode was accepted */
		if (response == 'C') {
			crcflag = 1;
		}
		
		/* got SOH/STX add to buff */
		xm_buf[0] = c;
		
		for (i = 1; i < (xm_pkt_size + crcflag + 4); i++) {
			timeout = XM_TIMEOUT_DELAY;
			while (timeout && (!sci_drdy())) {
				_delay_us(XM_RXSCAN_DELAY_US);
				timeout--;
			}
			
			if (timeout) {
				xm_buf[i] = sci_getData();
			} else {
				retry--;
				Debug_FlushBuffer();
				response = NAK;
				break;
			}	
		}

		/* Packet was too small, retry */
		if (i < (xm_pkt_size + crcflag + 4))
		continue;


		if ((xm_buf[1] == (uint8_t)(~xm_buf[2])) && xmodem_crc_check(crcflag, &xm_buf[3], xm_pkt_size)) {       /* Packet is ok */
			
			if (xm_buf[1] == xm_pkt_num) {      /* This is the correct packet */
				xm_data.ofs = ofs;
				xm_data.p_buf = &xm_buf[3];
				xm_data.len = xm_pkt_size;
				if (callback != NULL)
				{
					if (callback((void *)&xm_data)) {       /* Write success */
						(uint8_t)xm_pkt_num++;              /* Update packet number */
						ofs += xm_pkt_size;                 /* Update offset */
						retry = XM_RETRY_LIMIT;             /* Reset retries */
						response = ACK;
					}
					else {  /* Write failed */
						retry--;
						Debug_FlushBuffer();
						response = NAK;
					}
				}
				else {
					(uint8_t)xm_pkt_num++;                  /* Update packet number */
					ofs += xm_pkt_size;                     /* Update offset */
					retry = XM_RETRY_LIMIT;                 /* Reset retries */
					response = ACK;
				}
				continue;
			}
			else if (xm_buf[1] == (uint8_t)(xm_pkt_num - 1)){ /* This is a retransmission of the last packet */
				response = ACK;
				continue;
			}
			else {      /* Completely out of sync */
				Debug_FlushBuffer();
				sci_writeData(CAN);
				sci_writeData(CAN);
				sci_writeData(CAN);
				return (XM_ERR_SYNC);
			}

			}	else {      /* Packet was corrupt */
			retry--;
			Debug_FlushBuffer();
			response = NAK;
			continue;
		}				
	}

	/* Exceeded retry count */
	Debug_FlushBuffer();
	sci_writeData(CAN);
	sci_writeData(CAN);
	sci_writeData(CAN);
	
	return (XM_ERR_RTRY);
}




/******************************************************************************
 * xmodem_crc_check
 ******************************************************************************/
bool xmodem_crc_check(uint8_t crcflag, const uint8_t *pbyte, uint16_t len) {
	uint16_t crc = 0, crc_calc;
	uint8_t i, chksum = 0;

	if (crcflag) /* Do CRC16 checksum */
	{
		while (len--) {
			crc = crc ^ ((uint16_t) *pbyte++ << 8);
			for (i = 0; i < 8; i++) {
				if (crc & 0x8000)
					crc = (crc << 1) ^ 0x1021;
				else
					crc = (crc << 1);
			}
		}

		/* Check checksum against packet */
		crc_calc = ((uint16_t) *pbyte << 8);
		pbyte++;
		crc_calc |= (*pbyte & 0xFF);
		if (crc == crc_calc)
			return (true);
	} else /* Do regular checksum */
	{
		while (len--) {
			chksum += *pbyte++;
		}

		/* Check checksum against packet */
		if (chksum == pbyte[len])
			return (true);
	}

	return (false);
}

/******************************************************************************
 * xmodem_crc_calc
 ******************************************************************************/
uint16_t xmodem_crc_calc(uint8_t crcflag, const uint8_t *pbyte, uint16_t len) {
	uint16_t crc = 0;
	uint8_t i, chksum = 0;

	if (crcflag) /* Do CRC16 checksum */
	{
		while (len--) {
			crc = crc ^ ((uint16_t) *pbyte++ << 8);
			for (i = 0; i < 8; i++) {
				if (crc & 0x8000)
					crc = (crc << 1) ^ 0x1021;
				else
					crc = (crc << 1);
			}
		}

		return (crc);
	} else /* Do regular checksum */
	{
		while (len--) {
			chksum += *pbyte++;
		}

		return (chksum);
	}

	return (0);
}
