/*
 * BootBridge.c
 *
 * Created: 22-Mar-18 00:53:54
 *  Author: Willem van der Kooij
 */ 

#define F_CPU	32000000

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "BootBridge.h"
#include "SPI.h"
#include "Xmodem.h"
#include "UART_Debug.h"

uint8_t bootSlaveStatus = 0, bootSlaveID = 0, bootSlaveVersion = 0;

static void NRST_Toggling(void);
static bool BootBridgecallback(void *p_args);

//Handshake line = NCHG = I2S_FS

uint8_t SAMG55_BootProg(void)
{
	//uint8_t saveInterruptEnableState = (CCP.SREG & (0x01 << 7));
	uint8_t xmodemError = 5;
	uint16_t timeout = 0;
	
	NRST_Toggling();	//toggle target reset for SAMG55 to acces bootloader

	/* wait for SAMG55 to pull handshake line low */
	timeout = SAMG55_HANDSHAKE_TIMEOUT;
	while ((PORTC.IN & PIN2_bm) && timeout) {
		timeout--;
		_delay_us(10);
	}
	
	if (timeout) {
		//printf("Handshake received\r\n");
		
		/*
		printf("Status : 0x%X\r\n", SPI_xchg(0xFF));
		printf("ID : 0x%X\r\n", SPI_xchg(0xFF));
		printf("Version : 0x%X\r\n", SPI_xchg(0xFF));
		*/
		
		SPI_xchg(SAMG55_BOOT_LOCK1);			//Get Status byte
		printf("Status (0xE0) : 0x%X\r\n", SPI_xchg(0xFF)); //read status
		SPI_xchg(SAMG55_BOOT_LOCK2);			//not allowed to read status
		//unlock command is send
		
		//xmodemError = xmodem_receive(&BootBridgecallback, 0x00);			//write data to target
		//sei();	//re-enable interrupts after xmodemreceive function
		
		if (!xmodemError) {
			printf("xmodem transfer complete Bootloading succes!\r\n");
			return 0;
		} else {
			printf("Bootloading failed\r\n");
			return 1;
		}
		

		
	} else printf("No Handshake received\r\n");
	
	return 1;
}

static void NRST_Toggling(void)
{
	for (int i = 0; i < 10; i++) {
		PORTC.OUTCLR = PIN3_bm;
		_delay_ms(1);
		PORTC.OUTSET = PIN3_bm;
		_delay_ms(50);
	}
}


static bool BootBridgecallback(void *p_args)
{
	xm_data_t *xm_data = (xm_data_t*) p_args;
	
	PORTA.OUTTGL = PIN1_bm;
	
	for (uint16_t i = 0; i < (xm_data -> len); i++)	{
		//BootSpiXchg((xm_data -> p_buf[i]), NULL, (xm_data -> len));	//cant pass NULL parameter
		
		SPI_xchg(SAMG55_BOOT_LENGHT1);
		printf("Status (0xA0) : 0x%X\r\n", SPI_xchg(0xFF)); //read status
		SPI_xchg(SAMG55_BOOT_LENGHT2);
		//lenght bytes send
		
		SPI_xchg((xm_data -> p_buf[i]));
		PORTC.OUTSET = PIN4_bm;	//pull CSn high again?
	}
	
	return true;
}