/*
 * UART_Debug.c
 *
 * Created: 28-Jan-18 00:54:36
 *  Author: Willem van der Kooij
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>
#include <avr/interrupt.h>
#include <string.h>

#include "UART_Debug.h"

//baud calculated for 32MHz
#define UART_DEBUG_BAUD		115200
#define UART_DEBUG_BSEL		33
#define UART_DEBUG_BSCALE	0xFF	

#define TXBUF_DEPTH_CTRL	250
#define RXBUF_DEPTH_CTRL	80

static int Debug_Putchar(char c, FILE *stream);

FILE gDebug = FDEV_SETUP_STREAM(Debug_Putchar, NULL, _FDEV_SETUP_WRITE);

static volatile uint8_t tx_ctrl_wridx, tx_ctrl_rdidx, tx_ctrl_buf[TXBUF_DEPTH_CTRL];
static volatile uint8_t rx_ctrl_wridx, rx_ctrl_rdidx, rx_ctrl_buf[RXBUF_DEPTH_CTRL];


/*!
 * \brief Setup uart for debugging.
 */
void Init_UART(void)
{
	cli();
	
	USARTD0.CTRLB = 0;	//make sure bootloader hasn't left uart open

	PORTD.PIN2CTRL = PORT_OPC_PULLUP_gc;	//pull-up Rx
	
	PORTD.OUTSET = PIN3_bm;		//set TX as output
	PORTD.DIRSET = PIN3_bm;		
	PORTD.DIRCLR = PIN2_bm;		//set RX as input
	PORTD.OUTCLR = PIN2_bm;
	
	USARTD0.BAUDCTRLA = (UART_DEBUG_BSEL & USART_BSEL_gm);
	USARTD0.BAUDCTRLB = ((UART_DEBUG_BSCALE << USART_BSCALE_gp) & USART_BSCALE_gm) | ((UART_DEBUG_BSEL >> 8) & ~USART_BSCALE_gm);
	
	USARTD0.CTRLA = USART_RXCINTLVL_MED_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;
	USARTD0.CTRLB = USART_RXEN_bm | USART_TXEN_bm;		//enable TX & RX
	
	stdout = &gDebug;

	Debug_FlushBuffer();

	PMIC.CTRL |= PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm;
}


uint8_t CanRead_Ctrl(void) {
	uint8_t wridx = rx_ctrl_wridx, rdidx = rx_ctrl_rdidx;
	
	if(wridx >= rdidx)
		return wridx - rdidx;
	else
		return wridx - rdidx + RXBUF_DEPTH_CTRL;
	
}


uint8_t ReadByte_Ctrl(void) {
	uint8_t res, curSlot, nextSlot;
		
	curSlot = rx_ctrl_rdidx;
	// Busy-wait for a byte to be available. Should not be necessary if the caller calls CanRead_xxx() first 
	while(!CanRead_Ctrl()) ;
		
	res = rx_ctrl_buf[curSlot];

	nextSlot = curSlot + 1;
	if(nextSlot >= RXBUF_DEPTH_CTRL)
		nextSlot = 0;
	rx_ctrl_rdidx = nextSlot;
		
	return res;
}

uint8_t CanWrite_Ctrl(void) {
	uint8_t wridx1 = tx_ctrl_wridx + 1, rdidx = tx_ctrl_rdidx;
	
	if(wridx1 >= TXBUF_DEPTH_CTRL)
		wridx1 -= TXBUF_DEPTH_CTRL;
	if(rdidx >= wridx1)
		return rdidx - wridx1;
	else
		return rdidx - wridx1 + TXBUF_DEPTH_CTRL;
	
}


void WriteByte_Ctrl(uint8_t data) {
	uint8_t curSlot, nextSlot, savePMIC;
	
	// Busy-wait for a byte to be available. Should not be necessary if the caller calls CanWrite_xxx() first 
	while(!CanWrite_Ctrl())
		USARTD0.CTRLA = USART_RXCINTLVL_MED_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_LO_gc;
	
	curSlot = tx_ctrl_wridx;
	tx_ctrl_buf[curSlot] = data;
	
	nextSlot = curSlot + 1;
	if(nextSlot >= TXBUF_DEPTH_CTRL)
		nextSlot = 0;

	savePMIC = PMIC.CTRL;
	PMIC.CTRL = savePMIC & ~PMIC_LOLVLEN_bm;
	tx_ctrl_wridx = nextSlot;
	USARTD0.CTRLA = USART_RXCINTLVL_MED_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_LO_gc;
	PMIC.CTRL = savePMIC;

} 


static int Debug_Putchar(char c, FILE *stream)
{	
	WriteByte_Ctrl((uint8_t) c);
	
	return 0;
}

void Debug_FlushBuffer(void)
{
	uint8_t data = USARTD0.DATA;	//read reg to clear data
}

uint8_t sci_drdy(void)		//check if data has been received
{
	if ((USARTD0.STATUS & USART_RXCIF_bm) == USART_RXCIF_bm) {
		return 1;
	} else return 0;
}

uint8_t sci_getData(void)
{
	while(!( (USARTD0.STATUS & USART_RXCIF_bm) == USART_RXCIF_bm ));	//wait for data to come in
	
	return USARTD0.DATA;
}

void sci_writeData(uint8_t txdata)
{
	USARTD0.DATA = txdata;
	
	while (! ((USARTD0.STATUS & USART_TXCIF_bm) == USART_TXCIF_bm ));	//wait for data to be shifted out
}

ISR(USARTD0_RXC_vect) {
	
	uint8_t curSlot, nextSlot;
	
	curSlot = rx_ctrl_wridx;
	rx_ctrl_buf[curSlot] = USARTD0.DATA;
	
	nextSlot = curSlot + 1;
	if(nextSlot >= RXBUF_DEPTH_CTRL)
	nextSlot = 0;
	
	if(nextSlot != rx_ctrl_rdidx)
	rx_ctrl_wridx = nextSlot;
	
} 


ISR(USARTD0_DRE_vect) {
		
	uint8_t curSlot, nextSlot, lastSlot;
		
	nextSlot = curSlot = tx_ctrl_rdidx;
	lastSlot = tx_ctrl_wridx;
		
	if(curSlot != lastSlot) {
		USARTD0.DATA = tx_ctrl_buf[curSlot];
		nextSlot = curSlot + 1;
		if(nextSlot >= TXBUF_DEPTH_CTRL)
		nextSlot = 0;
	}
	if(nextSlot == lastSlot)
	USARTD0.CTRLA = USART_RXCINTLVL_MED_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;
		
	tx_ctrl_rdidx = nextSlot;	
} 
