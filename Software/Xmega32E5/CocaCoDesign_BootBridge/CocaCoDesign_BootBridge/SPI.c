/*
 * SPI.c
 *
 * Created: 18-Mar-18 00:03:53
 *  Author: Willem van der Kooij
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>
#include <avr/interrupt.h>

#include "SPI.h"

/*!
 * \brief Initialize SPI interface master mode
 * \details SPI Master Mode MSB-first, 8-bit, Mode 1, CLKDIV16
 */
void Init_SPI(void)
{
	PORTC.DIRSET = PIN4_bm | PIN5_bm | PIN7_bm;
	PORTC.DIRCLR = PIN6_bm;
	
	SPIC.INTCTRL =	0x00;	//interrupts disabled
	
	SPIC.CTRL	=	SPI_MASTER_bm
				|	SPI_ENABLE_bm
				|	SPI_MODE1_bm
				|	SPI_PRESCALER1_bm;	//prescaler DIV16
}

/*!
 * \brief transceive 1 byte
 */
uint8_t SPI_xchg(uint8_t txdata)
{
	//pull Csn Low
	PORTC.OUTCLR = PIN4_bm;
	
	SPIC.DATA = txdata;
	
	while(! ((SPIC.STATUS & SPI_IF_bm) == SPI_IF_bm)); //wait for IF bit to set
	
	//PORTC.OUTSET = PIN4_bm;
	
	return SPIC.DATA;	//return received data
}


/*!
 *\brief transceive multiple bytes at once
 */
void BootSpiXchg(uint8_t *txdata, uint8_t *rxdata, uint16_t len)
{
	//pull CSn low
	PORTC.OUTCLR = PIN4_bm;
	
	for (uint16_t i = 0; i < len; i++) {
		rxdata[i] = SPI_xchg(txdata[i]);
	}
	
	//pull CSn high
	PORTC.OUTSET = PIN4_bm;
}