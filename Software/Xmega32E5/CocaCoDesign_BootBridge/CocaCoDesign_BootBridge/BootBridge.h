/*
 * BootBridge.h
 *
 * Created: 22-Mar-18 00:54:18
 *  Author: Willem van der Kooij
 */ 


#ifndef BOOTBRIDGE_H_
#define BOOTBRIDGE_H_

uint8_t SAMG55_BootProg(void);

#define SAMG55_HANDSHAKE_TIMEOUT	1000
#define SAMG55_BOOT_LOCK1	0xDC
#define SAMG55_BOOT_LOCK2	0xAA
#define SAMG55_BOOT_LENGHT1	0x00
#define SAMG55_BOOT_LENGHT2 0xFF

//status codes
#define WAITING_BOOTLOADER_CMD	0xC0
#define WAITING_FRAME_DATA		0x80
#define FRAME_CRC_CHECK			0x02
#define FRAME_CRC_FAIL			0x03
#define	FRAME_CRC_PASS			0x04
#define	APP_CRC_FAIL			0x40
#define ERROR_DETECTED			0x06



#endif /* BOOTBRIDGE_H_ */