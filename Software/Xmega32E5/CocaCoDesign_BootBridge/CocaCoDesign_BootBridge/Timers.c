/*
 * Timers.c
 *
 * Created: 20-Mar-18 17:46:00
 *  Author: Willem van der Kooij
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>
#include <avr/interrupt.h>

#include "Timers.h"


void Init_TimerC4(void)
{
	TCC4.CTRLB		=	TC_WGMODE_NORMAL_gc;
	TCC4.CTRLA		=	TC_CLKSEL_DIV1024_gc;
	TCC4.INTCTRLA	=	TC_OVFINTLVL_LO_gc;
	TCC4.PER		=	10000;

	PMIC.CTRL		|=	PMIC_LOLVLEN_bm;
}

ISR(TCC4_OVF_vect)
{
	PORTA.OUTTGL = PIN1_bm;
	TCC4.CNT = 0;
}

