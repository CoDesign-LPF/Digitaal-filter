/*
 * Xmodem.h
 *
 * Created: 18-Mar-18 01:03:20
 *  Author: Willem van der Kooij
 */ 


#ifndef XMODEM_H_
#define XMODEM_H_

#include <stdbool.h>
#include <stddef.h>

//#define XM_BUFFER_SIZE		128
#define XM_BUFFER_SIZE		1024
#define XM_RETRY_LIMIT		60
#define XM_TIMEOUT_DELAY	10000
#define XM_RXSCAN_DELAY_US	50

//Xmodem Commands
#define SOH		0x01	//start of header 128 B
#define STX		0x02	//start of header 1 K
#define EOT		0x04	//End of Transmission
#define ACK		0x06	//Acknowledge
#define NAK		0x15	//Not Acknowledge
#define CAN		0x18	//Cancel (force Rx to start sending C's)
#define CTRLZ	0x1A	

// xmodem error codes
typedef enum
{
	XM_SUCCESS = 0,
	XM_ERR_CAN,
	XM_ERR_SYNC,
	XM_ERR_RTRY
} xm_err_t;

// Data structure
typedef struct
{
	uint32_t ofs;
	uint8_t *p_buf;
	uint16_t len;
} xm_data_t;

/* Function prototypes */
xm_err_t xmodem_receive(bool (*callback)(void *p_args), uint32_t offset);
bool xmodem_crc_check(uint8_t crcflag, const uint8_t *pbyte, uint16_t len);
uint16_t xmodem_crc_calc(uint8_t crcflag, const uint8_t *pbyte, uint16_t len);

#endif /* XMODEM_H_ */