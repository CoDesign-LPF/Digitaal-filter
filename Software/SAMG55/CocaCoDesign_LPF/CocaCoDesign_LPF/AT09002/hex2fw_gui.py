import sys, time
from os import listdir
from os.path import isfile, join
import os
if not('.' in sys.path): sys.path.append('.')

from Tkinter import *
from tkFileDialog import *
import tkMessageBox
from subprocess import Popen, PIPE

# thinking in tkinter http://www.ferg.org/thinking_in_tkinter/all_programs.html

class TheGui:
    def __init__(self, parent):
        #------- frmSetup ----------#
        self.frmSetup = Frame(parent, bd=5)
        self.frmSetup.pack()

        self.path = StringVar()
        os_path = os.path.dirname(os.path.abspath(__file__))
        self.path.set(os_path+"\\config\\")
        print self.path.get()

        try:
		files = [ f for f in listdir(self.path.get()) if isfile(join(self.path.get(),f)) ]
	except:
		try:
			self.path.set(os_path+"..\\config\\")
			files = [ f for f in listdir(self.path.get()) if isfile(join(self.path.get(),f)) ]
		except:
			print "Not able to find any config file, exiting..."
			sys.exit()

    	optionList = tuple(f for f in files)
    	self.v = StringVar()
    	self.v.set(optionList[0])
    	self.config_file = StringVar()

        self.menu = OptionMenu(self.frmSetup, self.v, *optionList, command=self.selRadio).pack()

	self.inChoices = (f for f in files)

        self.varRadio = IntVar()

        #------- frmSetup ----------#

        sep = Frame(parent, width=1, bd=5, bg='black')
        sep.pack(fill=X, expand=1)

        #------- frmIn ----------#
        # http://effbot.org/tkinterbook/tkinter-widget-styling.htm
        self.frmIn = Frame(parent, bd=5)
        self.frmIn.pack()

        self.lblIn = Label(self.frmIn, text='Input File', width=20)
        self.lblIn.pack(side=LEFT)

        self.inFilePath = StringVar() # http://effbot.org/tkinterbook/entry.htm
        self.entIn = Entry(self.frmIn, width=50, textvariable=self.inFilePath)
        self.entIn.pack(side=LEFT)

        self.btnIn = Button(self.frmIn, text='Browse', command=self.btnInBrowseClick)
        self.btnIn.pack(side=LEFT)
        #------- frmIn ----------#

        #------- frmOut ----------#
        self.frmOut = Frame(parent, bd=5)
        self.frmOut.pack()

        self.lblOut = Label(self.frmOut, text='Output File', width=20)
        self.lblOut.pack(side=LEFT)

        self.outFilePath = StringVar()
        self.entOut = Entry(self.frmOut, width=50, textvariable=self.outFilePath)
        self.entOut.pack(side=LEFT)

        self.btnOut = Button(self.frmOut, text='Browse', command=self.btnOutBrowseClick)
        self.btnOut.pack(side=LEFT)
        #------- frmOut ----------#

        sep = Frame(parent, width=1, bd=5, bg='black')
        sep.pack(fill=X, expand=1)

        #------- frmButtons ----------#
        self.frmOut = Frame(parent, bd=5)
        self.frmOut.pack()

        self.btnConvert = Button(self.frmOut, text='Convert', command=self.btnConvertClick)
        self.btnConvert.pack()


    #------- handle commands ----------#
    def selRadio(self, value):
        self.config_file = value

    def btnInBrowseClick(self):
        rFilepath = askopenfilename(defaultextension='*.bin',
            initialdir='.', initialfile='', parent=self.frmIn, title='select a file', filetypes=[("Hex Files","*.hex"),("Bin Files","*.bin")])
        self.inFilePath.set(rFilepath)
        print self.entIn.get()

    def btnOutBrowseClick(self):
        rFilepath = asksaveasfilename(defaultextension='*.bin',
            initialdir='.', initialfile='', parent=self.frmIn, title='select a file')
        self.outFilePath.set(rFilepath)
        print self.entOut.get()

    def btnConvertClick(self):
        #defClr = self.btnConvert.cget("bg")
        self.btnConvert.config(relief=SUNKEN)
        self.btnConvert.update()
        #print 'Convert from %s to %s' % (self.inChoices[self.varRadio.get()], self.inChoices[(self.varRadio.get()+1)%2])
        config_file = join(self.path.get(),self.v.get())
        input_file = self.inFilePath.get()
        output_file = self.outFilePath.get()
        # print config_file
        # print input_file
        # print output_file

        if output_file == "":
        	output_file = "std.bin"
        if input_file == "":
        	tkMessageBox.showerror("Error", "Must provide input file")
       	else:
		cmd = "hex2fw.py -i \""+input_file+"\" -o \""+output_file+"\" -c \""+config_file+"\""
		print cmd
		proc = Popen(cmd, shell=True, bufsize=1, stdout=PIPE)
		for line in proc.stdout:
		    if "% done" in line:
		    	number = ""
		    	for ch in line:
		    		if ch.isdigit():
		    			number+=ch
		    	print number

		# proc.stdout.close()
		# proc.wait()

		tkMessageBox.showinfo("Info", "Done converting, FW written to "+output_file)

        self.btnConvert.config(relief=RAISED)
        self.btnConvert.update()

root = Tk()
root.title("Convert between .hex/.bin and FW files")
#http://infohost.nmt.edu/tcc/help/pubs/tkinter/std-attrs.html#geometry
#http://infohost.nmt.edu/tcc/help/pubs/tkinter/toplevel.html
root.geometry("500x200+10+10")
gui = TheGui(root)
root.mainloop()