from os import listdir
from os.path import isfile, join
import os
import sys
import getopt
import subprocess

input_file = ""
output_file = ""
config_file = ""

NO = 0
YES = 1
ALL = 2

def calc_crc8(number):
	crc8 = 0
	for i in range(1, len(number)-3, 2):
		byte_x = int(number[i:i+2],16)
		crc8 += byte_x
	return hex((~crc8+1) & 0xff)

def check_crc(number):
	calc_crc = calc_crc8(number)
	given_crc = hex(int(number[len(number)-3:len(number)-1],16))
	if given_crc == calc_crc:
		return True
	else:
		return False

def record_16_crc(crc, ch):
	crcPoly = 0x8005

	m = (crc << 8) | ch

	for n in range(0,8):
		m = m << 1
		if (m & 0x1000000):
			m ^= (crcPoly << 8)

	return (m >> 8) & 0xffff

def get_crc16(frame):
	Frame_CRC_val = 0
	for j in range(2, len(frame)):
		Frame_CRC_val = record_16_crc(Frame_CRC_val, frame[j]);
	Frame_CRC_val = record_16_crc(Frame_CRC_val, 0);
	Frame_CRC_val = record_16_crc(Frame_CRC_val, 0);

	return Frame_CRC_val

def get_crc32(frame):
    return calc_end_crc(frame[2:], len(frame)-2)

def write_to_file(f, bfr):
	f.write(''.join(chr(b) for b in bfr))

def build_frame(address, data, size, data_type):
	global f
	global sign
	#Size of the record : 4(sig) + 1(data_type) + 3(add) + 2(size) + datasize + 1(EOL) + 2(crc)
	send_data = []
	if data_type == TYPE_DATA:
		length = size
	else:
		length = 0
    
	if CRC_BITS == 32:
		data_len = length+15
		while (data_len%4) != 0:
			data_len = data_len + 1
		send_data.append((data_len >> 8) & 0xff)
		send_data.append((data_len >> 0) & 0xff)
	else:
		send_data.append(((length+13) >> 8) & 0xff)
		send_data.append(((length+13) >> 0) & 0xff)
	send_data.extend(sign)
	send_data.append(data_type & 0xff)
	send_data.append((address >> 16) & 0xff)
	send_data.append((address >> 8) & 0xff)
	send_data.append((address >> 0) & 0xff)
	send_data.append((size >> 8) & 0xff)
	send_data.append((size >> 0) & 0xff)
	send_data.extend(data)
	send_data.append(TYPE_EOF & 0xff)

	if CRC_BITS == 32:
		while ((len(send_data)-2)%4) != 0:
			send_data.append(0)
		crc = get_crc32(send_data)
		send_data.append((crc >> 24) & 0xff)
		send_data.append((crc >> 16) & 0xff)
		send_data.append((crc >> 8) & 0xff)
		send_data.append(((crc >> 0) & 0xff))
	else:
		crc = get_crc16(send_data)
		send_data.append((crc >> 8) & 0xff)
		send_data.append((crc >> 0) & 0xff)

	write_to_file(f, send_data)

	return send_data

def generate_lut_entry(lut_index):
	crc_tab_entry = lut_index;
	for i in range(8, 0, -1):
		if (crc_tab_entry & 0x01):
			crc_tab_entry = (crc_tab_entry >> 1) ^ 0xEDB88320;

		else:
			crc_tab_entry = crc_tab_entry >> 1;
	return(crc_tab_entry);


def calc_end_crc(bfr, length):
	crc32 = 0xffffffff

	for i in range(0,length):
		data_byte = bfr[i];

		idx = (data_byte ^ (crc32 & 0x000000FF))

		# work out the CRC
		# the CRC function is the IEEE CRC32 function with a polynomial
		# of 0xEDB88320, implemented using a lookup table
		crc32 = (crc32 >> 8)

		crc32 = crc32 ^ generate_lut_entry(idx)

	return crc32


def usage():
	print
	print "================================================================================"
	print
	print "usage: hex2fw [options] [args]"
	print
	print "Available options"
	print "-h --help              show this help menu"
	print "-i [input file]        path to input file (.hex/.bin)"
	print "-o [output file]       path to output file"
	print "-c [config file]       path to config file for device (.icf)"
	print
	print "Example: hex2fw -i input_file.hex [-o output_file.bin [-c config_device.icf]]"
	print
	print "================================================================================"
	print
	sys.exit()


try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:c:", ["help"])
except getopt.GetoptError:
	usage()

if not 'PROMPT' in os.environ:
	print "================================================================================"
	print
	print "This is a command line tool, enter 'hex2fw --help' to view help"
	print
	print "================================================================================"
	sub = subprocess.Popen("cmd")

	sys.exit()

i = 0
for opt, arg in opts:
        if opt in ("-h", "--help"):
            	usage()
        elif opt in ("-i"):
        	input_file = arg

        elif opt in ("-o"):
        	if not arg:
        		output_file = "std.bin"
        	else:
        		output_file = arg

        elif opt in ("-c"):
        	config_file = arg
        else:
        	usage()

if not len(input_file):
	print "No input file provided (-h for help), exiting..."
	sys.exit()

if not output_file:
	output_file = "std.bin"

if not config_file:
	path = "config"
	try:
		files = [ f for f in listdir(path) if isfile(join(path,f)) ]
	except:
		try:
			path = "../config"
			files = [ f for f in listdir(path) if isfile(join(path,f)) ]
		except:
			print "Not able to find any config file, exiting..."
			sys.exit()

	i = 1
	print
	print "Configs found:"
	for f_name in files:
		print "\t%d." % i, f_name
		i += 1
	print
	config_file = raw_input("Select config (Or provide path to .icf file): ")
	print

if config_file.isdigit():
	execfile(path+"/"+files[int(config_file)-1])
else:
	try:
		path = "config"
		execfile(config_file)
	except:
		print "Not a valid config file"
		sys.exit()

sign = []
sign.append((SIGNATURE  >> 24) & 0xff)
sign.append((SIGNATURE >> 16) & 0xff)
sign.append((SIGNATURE >> 8) & 0xff)
sign.append((SIGNATURE >> 0) & 0xff)
print
try:
	print "Using config:"
	print "\tSIGNATURE:\t\t0x%x" % SIGNATURE
except:
	print "\tSIGNATURE NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tEND CRC:\t\t%s" % END_CRC
except:
	print "\tEND CRC NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tPAGE SIZE:\t\t0x%x" % TARGET_PAGE_SIZE
except:
	print "\tPAGE SIZE NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tTARGET_ROWS_PER_BLOCK:\t\t0x%x" % TARGET_ROWS_PER_BLOCK
except:
	print "\tTARGET_ROWS_PER_BLOCK NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tFLASH ADDRESS:\t\t0x%x" % TARGET_FLASH_ADDRESS
except:
	print "\tFLASH ADDRESS NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tFLASH SIZE:\t\t0x%x" % TARGET_FLASH_SIZE
except:
	print "\tFLASH SIZE NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tBOOTLOADER SIZE:\t0x%x" % TARGET_BOOTLOADER_SIZE
except:
	print "\tBOOTLOADER SIZE NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
	print "\tPacket CRC bits:\t\t%x" % CRC_BITS
except:
	print "\CRC BITS NOT FOUND"
	print "Failed to find needed config, exiting..."
	sys.exit()
try:
    print "\tVector offset:\t\t%x" % TARGET_VECTOR_OFFSET
except:
    print "\VECTOR OFFSET NOT FOUND"
    print "Failed to find needed config, exiting..."
    sys.exit()

DATA_RECORD = 0x00
EOF = 0x01
ESAR = 0x02
ELAR = 0x04
SLAR = 0x05
TYPE_ERASE = 0x01
TYPE_PROGRAM = 0x04
TYPE_RESET = 0x07
TYPE_DATA = 0x03
TYPE_LOCKBITS = 0x06
TYPE_LOCK_LOCKBITS = 0x08
TYPE_EOF = 0x00
TARGET_APPLICATION_SIZE = TARGET_FLASH_SIZE - TARGET_BOOTLOADER_SIZE

try:
	if input_file.split(".")[-1] == "hex":
		f_size = os.path.getsize(input_file)
		f = open(input_file, 'r')
	elif input_file.split(".")[-1] == "bin":
		f_size = os.path.getsize(input_file)
		f = open(input_file, 'rb')
except:
	print "Could not find input file, exiting..."
	sys.exit()

#reset = ser.read(2)
#ser.write(''.join(chr(b) for b in [0x43,0xbf]))

base_address = 0
current_address = 0
frame_start_address = 0
frame = []
datasize = 0
first = 1
done = 0

shift = 0

application = []
start_address = 0
if input_file.split(".")[-1] == "hex":
	lines = f.readlines()
	for line in lines:
		if not check_crc(line):
			print "CRC failed in hex file, exiting..."
			sys.exit()

		#print "%2.0f%% done\r" % (float(done)/f_size*100.),
		done += len(line)

		data_length = int(line[1:3],16)
		data_address = int(line[3:7], 16)
		data_type = int(line[7:9],16)
		data = []
		for i in xrange(9,data_length*2+9, 2):
			data.append(int(line[i:i+2],16))

		if data_type == DATA_RECORD:
			address = base_address + data_address

			if (address >= (TARGET_FLASH_ADDRESS+TARGET_FLASH_SIZE) or address < (TARGET_FLASH_ADDRESS+TARGET_BOOTLOADER_SIZE)) and not END_CRC:
				print "Address is outside of target flash area. Please make sure you have considered the bootloader in linker script, exiting..."
				sys.exit()

			if (address >= (TARGET_FLASH_ADDRESS+TARGET_FLASH_SIZE) or address < (TARGET_FLASH_ADDRESS+TARGET_BOOTLOADER_SIZE+shift)) and END_CRC:
				print "Address is outside of target flash area. Please make sure application is linked to start address "+str(hex(TARGET_FLASH_ADDRESS+TARGET_BOOTLOADER_SIZE+shift))+", exiting..."
				sys.exit()

			if first:
				first = 0
				start_address = address
				if (start_address != TARGET_FLASH_ADDRESS + TARGET_BOOTLOADER_SIZE) and (start_address != TARGET_FLASH_ADDRESS + TARGET_BOOTLOADER_SIZE + shift):
					print "Start address does not match target config, exiting..."
					sys.exit()

				current_address = address
			#raw_input("Any key....")

			for x in xrange(address-current_address):
				application.append(0xff)

			application.extend(data)

			current_address = address+data_length

		elif data_type == EOF:
			break

		elif data_type == ESAR:
			base_address = data[0] << 12 | data[1] << 4

		elif data_type == ELAR:
			base_address = data[0] << 24 | data[1] << 16

		elif data_type == SLAR:
			base_address = data[0] << 24 | data[1] << 16

elif input_file.split(".")[-1] == "bin":
	in_file = f.read()
	for byte in in_file:
		application.append(ord(byte))
	start_address = TARGET_FLASH_ADDRESS + TARGET_BOOTLOADER_SIZE
else:
	print "Not a valid file. Expected hex/bin, got %s. Exiting..." % input_file.split(".")[-1]
	sys.exit()

f.close()

while (len(application) % 32) != 28:
	application.append(0xff)

app_len = []
if END_CRC == YES:
	application[TARGET_VECTOR_OFFSET] = (int(len(application)+4) >> 0) & 0xff
	application[TARGET_VECTOR_OFFSET+1] = (int(len(application)+4) >> 8) & 0xff
	application[TARGET_VECTOR_OFFSET+2] = (int(len(application)+4) >> 16) & 0xff
	application[TARGET_VECTOR_OFFSET+3] = (int(len(application)+4) >> 24) & 0xff

	crc32 = calc_end_crc(application, len(application))

	application.append(0xff)
	application.append(0xff)
	application.append(0xff)
	application.append(0xff)

	application[-1] = (int((crc32 >> 24) & 0xff))
	application[-2] = (int((crc32 >> 16) & 0xff))
	application[-3] = (int((crc32 >> 8) & 0xff))
	application[-4] = (int((crc32 >> 0) & 0xff))

if len(application) > TARGET_FLASH_SIZE - TARGET_BOOTLOADER_SIZE:
	print "Application to big for selected device."
	sys.exit()

data = []
if path == "config":
	f = open(output_file, 'wb')
else:
	f = open("../"+output_file, 'wb')
if not END_CRC:
	current_address = start_address
else:
	current_address = start_address - shift

current_page = 0
lock_address = current_address

if CLEAR_LOCK_BITS == YES:
    app_pages = len(application)/TARGET_PAGE_SIZE
    if len(application)%TARGET_PAGE_SIZE > 0:
        app_pages +=1
    build_frame(current_address, [], app_pages, TYPE_UNLOCKBITS)
    print "Unlocking 0x%x pages from"  % app_pages,
    print "address: 0x%0x"  % current_address
elif CLEAR_LOCK_BITS == ALL:
    target_pages = TARGET_APPLICATION_SIZE/TARGET_PAGE_SIZE
    if TARGET_APPLICATION_SIZE%TARGET_PAGE_SIZE > 0:
        target_pages +=1
    build_frame(current_address, [], target_pages, TYPE_UNLOCKBITS)
    print "Unlocking 0x%x pages from"  % target_pages,
    print "address: 0x%0x"  % current_address
#else:
#	build_frame((0x402000 + 0x2000 - 1) , [], 2, TYPE_UNLOCKBITS)
#	print "Unlocking 0x%x pages from"  % 2,
#	print "address: 0x%0x"  % (0x402000 + 0x2000 - 1)
        
for i in range(0, len(application)):
    if i % TARGET_PAGE_SIZE == 0 and i > 0:
        if not current_page % TARGET_ROWS_PER_BLOCK:
            build_frame(current_address, [], len(data), TYPE_ERASE)
        current_page += 1
        if sum(data) < 0xff * TARGET_PAGE_SIZE:
            build_frame(current_address, data, len(data), TYPE_DATA)
            build_frame(current_address, [], len(data), TYPE_PROGRAM)
        if END_CRC:
            current_address = start_address + i - shift
        else:
            current_address = start_address + i
        data = []
        print "%2.0f%% done\r" % (float(i)/len(application)*100.0),
    data.append(application[i] & 0xff)

if not current_page % TARGET_ROWS_PER_BLOCK:
    build_frame(current_address, [], len(data), TYPE_ERASE)
build_frame(current_address, data, len(data), TYPE_DATA)
build_frame(current_address, [], len(data), TYPE_PROGRAM)

if SET_LOCK_BITS == YES:
    app_pages = len(application)/TARGET_PAGE_SIZE
    if len(application)%TARGET_PAGE_SIZE > 0:
        app_pages +=1
    build_frame(lock_address, [], app_pages, TYPE_LOCK_LOCKBITS)
    print "Locking 0x%x pages from"  % app_pages,
    print "address: 0x%0x"  % lock_address
elif SET_LOCK_BITS == ALL:
    target_pages = TARGET_APPLICATION_SIZE/TARGET_PAGE_SIZE
    if TARGET_APPLICATION_SIZE%TARGET_PAGE_SIZE > 0:
        target_pages +=1
    build_frame(lock_address, [], target_pages, TYPE_LOCK_LOCKBITS)
    print "Locking 0x%x pages from"  % target_pages,
    print "address: 0x%0x"  % lock_address

build_frame(0, [], 0, TYPE_RESET)

print "%2.0f%% done\r" % 100.0
print
print "FW file written to", output_file
f.close()